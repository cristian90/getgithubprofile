package com.example.personal.getgithubprofile

import com.example.personal.getgithubprofile.data.dao.ProfileDao
import com.example.personal.getgithubprofile.data.model.ProfileData
import com.example.personal.getgithubprofile.net.ProfileDataClient
import com.example.personal.getgithubprofile.ui.MainRepository
import com.example.personal.getgithubprofile.util.TestServer
import com.example.personal.getgithubprofile.util.TrampolineSchedulerRule
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import org.mockito.Mockito.`when` as mWhen

@RunWith(MockitoJUnitRunner::class)
class MainTest {

    @Rule
    @JvmField
    val trampolineSchedulerRule: TrampolineSchedulerRule = TrampolineSchedulerRule()

    @Mock
    lateinit var dao: ProfileDao

    lateinit var profileDataClient: ProfileDataClient

    lateinit var repository: MainRepository

    @Before
    fun setup(){
        val retrofit =
                Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                        .baseUrl(TestServer.httpUrl)
                        .build()
        profileDataClient = retrofit.create(ProfileDataClient::class.java)
        repository = MainRepository(dao,profileDataClient)
    }

    @Test
    fun testUserOffline(){
        val profile = ProfileData("cristian","","")
        mWhen(dao.getUniqueRegister()).thenReturn(Flowable.just(profile))
        repository.getUserOffline()
                .test()
                .assertValue{it.name == "cristian"}
    }

    @Test
    fun TestUserOnline(){
        repository.getUserProfile("cristian")
                .test()
                .assertValue{ it.name == "cristian david"}
    }

    @Test
    fun noUserFound(){
        repository.getUserProfile("")
                .test()
                .assertError{
                    it as HttpException
                    it.code() == 404
                }
    }


}