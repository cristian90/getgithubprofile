package com.example.personal.getgithubprofile.util

import com.example.personal.getgithubprofile.data.model.ProfileData
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.HttpUrl
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest

object TestServer {

    val httpUrl: HttpUrl by lazy { prepareServer() }
    private val gson: Gson = Gson()


    private fun prepareServer(): HttpUrl {
        val server: MockWebServer = MockWebServer()

        val dispatcher: Dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                "/users/cristian" -> {
                    val profile = ProfileData("cristian david","","")
                    MockResponse().setResponseCode(200)
                            .setBody(gson.toJson(profile))
                }
                else -> MockResponse().setResponseCode(404)
            }
        }

        server.setDispatcher(dispatcher)

        return server.url("/")
    }

}