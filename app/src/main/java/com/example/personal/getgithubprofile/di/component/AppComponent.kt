package com.example.personal.getgithubprofile.di.component

import android.app.Application
import com.example.personal.getgithubprofile.App
import com.example.personal.getgithubprofile.di.ActivityBuilderModule
import com.example.personal.getgithubprofile.di.module.AppModule
import com.example.personal.getgithubprofile.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivityBuilderModule::class
))
interface AppComponent{
    fun inject(app:App)

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun applicacion(application: Application):Builder
        fun build():AppComponent
    }
}