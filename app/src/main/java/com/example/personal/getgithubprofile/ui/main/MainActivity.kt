package com.example.personal.getgithubprofile.ui.main

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import com.example.cristiandev.utils.LifeDisposable
import com.example.cristiandev.utils.subscribeByShot
import com.example.cristiandev.utils.text
import com.example.personal.getgithubprofile.R
import com.example.personal.getgithubprofile.data.model.ProfileData
import com.example.personal.getgithubprofile.databinding.ActivityMainBinding
import com.example.personal.getgithubprofile.di.Injectable
import com.example.personal.getgithubprofile.ui.detail.DetailActivity
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_progress_dialog.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import javax.inject.Inject

class MainActivity @Inject constructor() : AppCompatActivity(), Injectable {

    @Inject
    lateinit var viewModel: MainViewModel

    val progress: Dialog by lazy { Dialog(this) }

    val dis = LifeDisposable(this)

    lateinit var binding: ActivityMainBinding
    lateinit var user:ProfileData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        progress.setContentView(R.layout.custom_progress_dialog)
        progress.setCancelable(false)
        viewModel.getUserOffline()
                .subscribe {
                    Log.i("asd",it.name)
                    if(it!=null) {
                        binding.user = it
                        binding.result = true
                        user = it
                    }
                }
        btnSearchUser.animation = AnimationUtils.loadAnimation(this,R.anim.anim_btn_search)
    }

    override fun onResume() {
        super.onResume()

        dis add btnSearchUser.clicks()
                .subscribe {
                    progress.show()
                    dis add viewModel.getUserProfile(usernameSearch.text())
                            .subscribeByShot(
                                    onNext = {
                                        progress.dismiss()
                                        binding.user = it
                                        binding.result = true
                                        user = it
                                    },
                                    onError = {
                                        progress.dismiss()
                                        Log.e("OnError", it.message)
                                    },
                                    onHttpError = {
                                        binding.result = false
                                        progress.dismiss()
                                        Log.e("OnHttpError", resources.getString(it))
                                        if(it!=R.string.http_404) toast(it) //debido a que el api de github devuelve 404 si el usuario no existe, por lo que no lo considero un error.
                                    }
                            )
                }
        dis add userContainer.clicks()
                .subscribe { startActivity<DetailActivity>(USER to user) }
    }

    companion object {
        const val USER = "user"
    }

}
