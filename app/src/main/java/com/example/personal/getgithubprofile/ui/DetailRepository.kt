package com.example.personal.getgithubprofile.ui

import com.example.cristiandev.utils.applySchedulers
import com.example.personal.getgithubprofile.data.dao.ProfileDao
import com.example.personal.getgithubprofile.data.model.ProfileData
import io.reactivex.Observable
import javax.inject.Inject

class DetailRepository @Inject constructor(private val dao: ProfileDao) {

    fun insertUserToBd(user: ProfileData): Observable<Boolean> =
            Observable.fromCallable {
                dao.insert(user)
                true
            }.applySchedulers()

}