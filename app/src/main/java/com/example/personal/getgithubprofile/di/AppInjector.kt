package com.example.personal.getgithubprofile.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.example.personal.getgithubprofile.App
import com.example.personal.getgithubprofile.di.component.DaggerAppComponent
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

class AppInjector {
    companion object {
        fun init(app: App){
            DaggerAppComponent.builder()
                    .applicacion(app)
                    .build()
                    .inject(app)
            app.registerActivityLifecycleCallbacks(object: Application.ActivityLifecycleCallbacks{
                override fun onActivityPaused(p0: Activity?) {}

                override fun onActivityResumed(p0: Activity?) {}

                override fun onActivityStarted(p0: Activity?) {}

                override fun onActivityDestroyed(p0: Activity?) {}

                override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {}

                override fun onActivityStopped(p0: Activity?) {}

                override fun onActivityCreated(p0: Activity, p1: Bundle?) {
                    handlerActivity(p0)
                }

            })
        }
        private fun handlerActivity(activity: Activity){
            if(activity is Injectable || activity is HasSupportFragmentInjector) AndroidInjection.inject(activity)

            (activity as AppCompatActivity).supportFragmentManager
                    .registerFragmentLifecycleCallbacks(object:FragmentManager.FragmentLifecycleCallbacks(){
                        override fun onFragmentCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
                            if(f is Injectable) AndroidSupportInjection.inject(f)
                        }
                    },true)
        }
    }
}