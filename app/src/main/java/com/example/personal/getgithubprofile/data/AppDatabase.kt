package com.example.personal.getgithubprofile.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverter
import com.example.personal.getgithubprofile.data.dao.ProfileDao
import com.example.personal.getgithubprofile.data.model.ProfileData
import java.util.*

@Database(entities = arrayOf(ProfileData::class),version = 2)
abstract class AppDatabase:RoomDatabase() {
    abstract fun profileDao(): ProfileDao
}