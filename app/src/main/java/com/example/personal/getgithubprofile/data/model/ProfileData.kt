package com.example.personal.getgithubprofile.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
class ProfileData(@PrimaryKey var idUSer: Long?,
                  var name: String?,
                  @SerializedName("avatar_url")
                  var avatarUrl: String?,
                  var bio: String?):Parcelable{
    @Ignore
    constructor(name: String,avatarUrl: String,bio: String):
            this(null,name,avatarUrl,bio)
}