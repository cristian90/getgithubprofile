package com.example.personal.getgithubprofile.di

import javax.inject.Scope

@Retention
@Scope
annotation class ActivityScope