package com.example.personal.getgithubprofile.ui.detail

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.cristiandev.utils.LifeDisposable
import com.example.personal.getgithubprofile.R
import com.example.personal.getgithubprofile.data.model.ProfileData
import com.example.personal.getgithubprofile.databinding.ActivityDetailBinding
import com.example.personal.getgithubprofile.di.Injectable
import com.example.personal.getgithubprofile.ui.main.MainActivity.Companion.USER
import javax.inject.Inject

class DetailActivity @Inject constructor() : AppCompatActivity(), Injectable {

    @Inject
    lateinit var viewModel: DetailViewModel

    val user: ProfileData by lazy { intent.extras.getParcelable<ProfileData>(USER) }
    lateinit var binding: ActivityDetailBinding
    val dis = LifeDisposable(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.user = user
        if (user.idUSer == null) dis add viewModel.insertUserToBd(user)
                .subscribe()
    }
}
