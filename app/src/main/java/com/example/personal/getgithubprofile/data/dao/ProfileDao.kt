package com.example.personal.getgithubprofile.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.provider.ContactsContract
import com.example.personal.getgithubprofile.data.model.ProfileData
import io.reactivex.Flowable

@Dao
interface ProfileDao {
    @Insert
    fun insert(profileData: ProfileData)

    @Query("SELECT * from profiledata ORDER BY idUSer DESC LIMIT 1")
    fun getUniqueRegister():Flowable<ProfileData>
}