package com.example.personal.getgithubprofile.ui

import com.example.cristiandev.utils.applySchedulers
import com.example.personal.getgithubprofile.data.dao.ProfileDao
import com.example.personal.getgithubprofile.data.model.ProfileData
import com.example.personal.getgithubprofile.net.ProfileDataClient
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

class MainRepository @Inject constructor(private val dao: ProfileDao, private val client: ProfileDataClient) {

    fun getUserProfile(username: String): Observable<ProfileData> =
            client.getGithubProfile(username)
                    .applySchedulers()

    fun getUserOffline(): Flowable<ProfileData> =
            dao.getUniqueRegister()
                    .applySchedulers()

}