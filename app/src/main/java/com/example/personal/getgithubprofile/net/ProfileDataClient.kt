package com.example.personal.getgithubprofile.net

import com.example.personal.getgithubprofile.data.model.ProfileData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ProfileDataClient {
    @GET("users/{username}")
    fun getGithubProfile(@Path("username") username:String):Observable<ProfileData>

}