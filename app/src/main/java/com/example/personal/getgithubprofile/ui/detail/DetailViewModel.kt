package com.example.personal.getgithubprofile.ui.detail

import android.arch.lifecycle.ViewModel
import com.example.personal.getgithubprofile.data.dao.ProfileDao
import com.example.personal.getgithubprofile.data.model.ProfileData
import com.example.personal.getgithubprofile.ui.DetailRepository
import io.reactivex.Observable
import javax.inject.Inject

class DetailViewModel @Inject constructor(val repository: DetailRepository) : ViewModel() {

    fun insertUserToBd(user: ProfileData): Observable<Boolean> =
            repository.insertUserToBd(user)
}