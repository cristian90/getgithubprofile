package com.example.cristiandev.pelicula.utils

import android.databinding.BindingAdapter
import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso

@BindingAdapter("app:imgUrl")
fun setImageUrl(img: ImageView, url: String?) {
    if(url!=null){
        Picasso.get()
                .load(Uri.parse(url))
                .into(img)
    }
}