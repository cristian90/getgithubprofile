package com.example.personal.getgithubprofile.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.example.personal.getgithubprofile.data.AppDatabase
import com.example.personal.getgithubprofile.data.dao.ProfileDao
import com.example.personal.getgithubprofile.net.ProfileDataClient
import com.example.personal.getgithubprofile.ui.DetailRepository
import com.example.personal.getgithubprofile.ui.MainRepository
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    fun provideDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, "app.db")
                    .fallbackToDestructiveMigration()
                    .build()

    @Singleton
    @Provides
    fun provideProfileDao(appDatabase: AppDatabase): ProfileDao =
            appDatabase.profileDao()

    @Provides
    @Singleton
    fun provideRetrofit(context: Context): Retrofit =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .baseUrl("https://api.github.com/")
                    .build()

    @Provides
    @Singleton
    fun provideProfileDataClient(retrofit: Retrofit): ProfileDataClient =
            retrofit.create(ProfileDataClient::class.java)

    @Provides
    @Singleton
    fun provideMainRepository(profileDao: ProfileDao, profileDataClient: ProfileDataClient) =
            MainRepository(profileDao, profileDataClient)

    @Provides
    @Singleton
    fun provideDetailRepository(profileDao: ProfileDao) =
            DetailRepository(profileDao)

}