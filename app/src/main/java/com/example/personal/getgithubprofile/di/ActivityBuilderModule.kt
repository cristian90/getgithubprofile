package com.example.personal.getgithubprofile.di

import com.example.personal.getgithubprofile.ui.detail.DetailActivity
import com.example.personal.getgithubprofile.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector()
    abstract fun bindMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector()
    abstract fun bindDetailActivity(): DetailActivity

}