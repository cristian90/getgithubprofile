package com.example.personal.getgithubprofile.ui.main

import android.arch.lifecycle.ViewModel
import com.example.personal.getgithubprofile.data.dao.ProfileDao
import com.example.personal.getgithubprofile.data.model.ProfileData
import com.example.personal.getgithubprofile.net.ProfileDataClient
import com.example.personal.getgithubprofile.ui.MainRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

class MainViewModel @Inject constructor(val repository: MainRepository) : ViewModel() {


    fun getUserProfile(username: String): Observable<ProfileData> =
            repository.getUserProfile(username)


    fun getUserOffline(): Flowable<ProfileData> =
            repository.getUserOffline()


}